const prevButton = document.querySelector('.prev');
const nextButton = document.querySelector('.next');
const slides = document.querySelectorAll('.slideshow-container > div');
const dots = document.querySelectorAll('.dots span');

let currentSlideIndex = 0;

function showSlide(slideIndex) {
    slides.forEach(slide => {
        slide.style.opacity = 0;
        slide.style.display = 'none';
    });
    slides[slideIndex].style.display = 'block';
    let opacity = 0;
    const intervalId = setInterval(() => {
        opacity += 0.1;
        slides[slideIndex].style.opacity = opacity;
        if (opacity >= 1) {
            clearInterval(intervalId);
        }
    }, 50);
    dots.forEach(dot => dot.classList.remove('active'));
    dots[slideIndex].classList.add('active');
}

function nextSlide() {
    currentSlideIndex++;
    if (currentSlideIndex >= slides.length) {
        currentSlideIndex = 0;
    }
    showSlide(currentSlideIndex);
}

function prevSlide() {
    currentSlideIndex--;
    if (currentSlideIndex < 0) {
        currentSlideIndex = slides.length - 1;
    }
    showSlide(currentSlideIndex);
}

prevButton.addEventListener('click', prevSlide);
nextButton.addEventListener('click', nextSlide);
dots.forEach((dot, index) => dot.addEventListener('click', () => showSlide(index)));

showSlide(currentSlideIndex);